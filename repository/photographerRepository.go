package repository

import (
	"context"
	"log"

	"gitlab.com/Piyali55-hub/photographer-app/entity"
	"gitlab.com/Piyali55-hub/photographer-app/view"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type PhotographerRepo struct {
	DBCollection *mongo.Collection
	EventTypeIdx string
}

func NewPhotographerRepo(dbCollection *mongo.Collection, eventTypeIdx string) Repository {
	return &PhotographerRepo{
		DBCollection: dbCollection,
		EventTypeIdx: eventTypeIdx,
	}
}

func (phRepo *PhotographerRepo) FindAll(pageNumber int, size int) ([]entity.Photgrapher, int, error) {
	var photographers []entity.Photgrapher
	filter := bson.D{}

	countOpts := options.Count().SetHint("_id_")
	count, err := phRepo.DBCollection.CountDocuments(context.TODO(), filter, countOpts)
	if err != nil {
		log.Printf("database error %s\n", err.Error())
		return photographers, int(count), err
	}

	queryOpts := options.Find().SetSort(bson.D{{"_id", 1}}).SetSkip(int64(pageNumber-1) * int64(size)).SetLimit(int64(size))
	cursor, err := phRepo.DBCollection.Find(context.TODO(), filter, queryOpts)

	if err != nil {
		log.Printf("database error %s\n", err.Error())
		return photographers, int(count), err
	}

	if err = cursor.All(context.TODO(), &photographers); err != nil {
		log.Printf("Error in accessing result %s\n", err)
		return photographers, int(count), err
	}
	return photographers, int(count), err
}

func (phRepo *PhotographerRepo) FindById(photographerId int) (entity.Photgrapher, error) {
	filter := bson.D{{"_id", photographerId}}
	var result entity.Photgrapher
	err := phRepo.DBCollection.FindOne(context.TODO(), filter).Decode(&result)

	if err != nil {
		log.Printf("database erre %s\n", err.Error())
		return result, err
	}
	return result, err
}

func (phRepo *PhotographerRepo) FindByEventType(eventType string) ([]view.PhotgrapherProjectionView, error) {
	var results []view.PhotgrapherProjectionView
	filter := bson.D{{"event_type.type", bson.D{{"$all", bson.A{eventType}}}}}
	opts := options.Find().SetProjection(bson.D{{"_id", 1}, {"first_name", 1}, {"last_name", 1}, {"avatar", 1}}).SetHint(phRepo.EventTypeIdx)
	cursor, err := phRepo.DBCollection.Find(context.TODO(), filter, opts)
	if err != nil {
		log.Printf("database erre %s\n", err.Error())
		return results, err
	}

	if err = cursor.All(context.TODO(), &results); err != nil {
		log.Printf("Error in accessing result %s\n", err)
		return results, err
	}
	return results, err
}
