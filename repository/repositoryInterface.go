package repository

import (
	"gitlab.com/Piyali55-hub/photographer-app/entity"
	"gitlab.com/Piyali55-hub/photographer-app/view"
)

type Repository interface {
	FindAll(pageNumber int, size int) ([]entity.Photgrapher, int, error)
	FindById(photographerId int) (entity.Photgrapher, error)
	FindByEventType(eventType string) ([]view.PhotgrapherProjectionView, error)
}
