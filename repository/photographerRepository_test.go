package repository

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Piyali55-hub/photographer-app/entity"
	"gitlab.com/Piyali55-hub/photographer-app/view"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/integration/mtest"
)

func TestFindById(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	defer mt.ClearCollections()
	mt.Run("success", func(mt *mtest.T) {
		dbCollection := mt.Coll
		phRepo := NewPhotographerRepo(dbCollection, "eventTypeIdx")
		expectedPhotographer := entity.Photgrapher{
			Id:        4072,
			Uid:       "c7204a53-77be-4ba6-b446-8f92921060a0",
			Password:  "uDQXExV6pr",
			FirstName: "Lorenzo",
			LastName:  "Fay",
			Username:  "lorenzo.fay",
			Email:     "lorenzo.fay@email.com",
			Avatar:    "https://robohash.org/autconsecteturlabore.png?size=300x300&set=set1",
		}

		mt.AddMockResponses(mtest.CreateCursorResponse(1, "foo.bar", mtest.FirstBatch, bson.D{
			{"_id", expectedPhotographer.Id},
			{"uid", expectedPhotographer.Uid},
			{"password", expectedPhotographer.Password},
			{"first_name", expectedPhotographer.FirstName},
			{"last_name", expectedPhotographer.LastName},
			{"username", expectedPhotographer.Username},
			{"email", expectedPhotographer.Email},
			{"avatar", expectedPhotographer.Avatar},
		}))
		actualPhotographer, err := phRepo.FindById(expectedPhotographer.Id)
		assert.Nil(t, err)
		assert.Equal(t, expectedPhotographer, actualPhotographer)
	})
}

func TestFindAll(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	defer mt.ClearCollections()
	mt.Run("success", func(mt *mtest.T) {
		dbCollection := mt.Coll
		phRepo := NewPhotographerRepo(dbCollection, "eventTypeIdx")

		p1 := entity.Photgrapher{
			Id:        4072,
			Uid:       "c7204a53-77be-4ba6-b446-8f92921060a0",
			Password:  "uDQXExV6pr",
			FirstName: "Lorenzo",
			LastName:  "Fay",
			Username:  "lorenzo.fay",
			Email:     "lorenzo.fay@email.com",
			Avatar:    "https://robohash.org/autconsecteturlabore.png?size=300x300&set=set1",
		}

		p2 := entity.Photgrapher{
			Id:        4073,
			Uid:       "e70098c4-ffe4-4c57-a508-c9f3d1a93822",
			Password:  "NWhCXfKbTU",
			FirstName: "Karey",
			LastName:  "Kuhic",
			Username:  "karey.kuhic",
			Email:     "karey.kuhic@email.com",
			Avatar:    "https://robohash.org/sedremquis.png?size=300x300&set=set1",
		}

		countResponse := mtest.CreateCursorResponse(1, "foo.bar", mtest.FirstBatch, bson.D{{"n", 2}})

		first := mtest.CreateCursorResponse(1, "foo.bar", mtest.FirstBatch, bson.D{
			{"_id", p1.Id},
			{"uid", p1.Uid},
			{"password", p1.Password},
			{"first_name", p1.FirstName},
			{"last_name", p1.LastName},
			{"username", p1.Username},
			{"email", p1.Email},
			{"avatar", p1.Avatar},
		})

		second := mtest.CreateCursorResponse(1, "foo.bar", mtest.NextBatch, bson.D{
			{"_id", p2.Id},
			{"uid", p2.Uid},
			{"password", p2.Password},
			{"first_name", p2.FirstName},
			{"last_name", p2.LastName},
			{"username", p2.Username},
			{"email", p2.Email},
			{"avatar", p2.Avatar},
		})

		killCursors := mtest.CreateCursorResponse(0, "foo.bar", mtest.NextBatch)

		mt.AddMockResponses(countResponse)
		mt.AddMockResponses(first, second, killCursors)
		photographers, count, err := phRepo.FindAll(1, 2)
		assert.Nil(t, err)
		assert.Equal(t, 2, count)
		assert.Equal(t, p1, photographers[0])
		assert.Equal(t, p2, photographers[1])
	})
}

func TestFindByEventType(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	defer mt.ClearCollections()
	mt.Run("success", func(mt *mtest.T) {
		dbCollection := mt.Coll
		phRepo := NewPhotographerRepo(dbCollection, "eventTypeIdx")
		p1 := view.PhotgrapherProjectionView{
			Id:        4072,
			FirstName: "Lorenzo",
			LastName:  "Fay",
			Avatar:    "https://robohash.org/autconsecteturlabore.png?size=300x300&set=set1",
		}

		p2 := view.PhotgrapherProjectionView{
			Id:        4073,
			FirstName: "Karey",
			LastName:  "Kuhic",
			Avatar:    "https://robohash.org/sedremquis.png?size=300x300&set=set1",
		}

		first := mtest.CreateCursorResponse(1, "foo.bar", mtest.FirstBatch, bson.D{
			{"_id", p1.Id},
			{"first_name", p1.FirstName},
			{"last_name", p1.LastName},
			{"avatar", p1.Avatar},
		})

		second := mtest.CreateCursorResponse(1, "foo.bar", mtest.NextBatch, bson.D{
			{"_id", p2.Id},
			{"first_name", p2.FirstName},
			{"last_name", p2.LastName},
			{"avatar", p2.Avatar},
		})

		killCursors := mtest.CreateCursorResponse(0, "foo.bar", mtest.NextBatch)

		mt.AddMockResponses(first, second, killCursors)
		phViews, err := phRepo.FindByEventType("wedding")
		assert.Nil(t, err)
		assert.Equal(t, p1, phViews[0])
		assert.Equal(t, p2, phViews[1])
	})
}
