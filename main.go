package main

import (
	"os"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gitlab.com/Piyali55-hub/photographer-app/controllers"
	_ "gitlab.com/Piyali55-hub/photographer-app/docs"
	"gitlab.com/Piyali55-hub/photographer-app/initializers"
	"gitlab.com/Piyali55-hub/photographer-app/repository"
	"go.mongodb.org/mongo-driver/mongo"
)

var dbCollection *mongo.Collection

func init() {
	initializers.LoadEnvVars()
	DB_URL := os.Getenv("DB_URL")
	dbCollection = initializers.OpenConnection(DB_URL)
}

func setUpPhotographerRoutes(router *gin.Engine, controller *controllers.PhotographerController) {
	api := router.Group("/api/photographers")
	{
		api.GET("/", controller.FindAll)
		api.GET("/:photographerID", controller.FindById)
		api.GET("/event/:eventType", controller.FindByEventType)
	}
}

// @title           Photographer API
// @version         1.0
// @description     This is a simple photographer app
// @termsOfService  http://swagger.io/terms/

// @contact.name   API Support
// @contact.url    http://www.swagger.io/support
// @contact.email  support@swagger.io

// @license.name  Apache 2.0
// @license.url   http://www.apache.org/licenses/LICENSE-2.0.html

// @host      localhost:3000
// @BasePath  /api

// @externalDocs.description  OpenAPI
// @externalDocs.url          https://swagger.io/resources/open-api/

func main() {
	repository := repository.NewPhotographerRepo(dbCollection, os.Getenv("INDEX_NAME"))
	controller := controllers.NewPhotographerController(repository)
	router := gin.Default()
	setUpPhotographerRoutes(router, controller)
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	router.Run()

	defer initializers.CleanUp()
}
