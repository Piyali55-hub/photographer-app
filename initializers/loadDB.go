package initializers

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var DBClient *mongo.Client

const (
	COLLECTION = "photographers"
	DATABASE   = "photograpgers_app_db"
)

func OpenConnection(DB_URL string) *mongo.Collection {
	var err error
	DBClient, err = mongo.Connect(context.TODO(), options.Client().ApplyURI(DB_URL))
	if err != nil {
		log.Fatalf("Can't connect to DB. Error %s\n", err.Error())
	}

	dBCollection := DBClient.Database(DATABASE).Collection(COLLECTION)
	return dBCollection
}

func CleanUp() {
	if err := DBClient.Disconnect(context.TODO()); err != nil {
		log.Printf("Error while disconnecting DB %s\n", err.Error())
		panic(err)
	}
}
