package view

import "gitlab.com/Piyali55-hub/photographer-app/entity"

type PhotgrapherProjectionView struct {
	Id        int    `json:"id" bson:"_id"`
	FirstName string `json:"first_name" bson:"first_name"`
	LastName  string `json:"last_name" bson:"last_name"`
	Avatar    string `json:"avatar"`
}

type PhotographerAllView struct {
	TotalRecords int                  `json:"totalRecords"`
	Page         int                  `json:"page"`
	Size         int                  `json:"size"`
	Data         []entity.Photgrapher `json:"data"`
}

type PhotographerQueryView struct {
	TotalRecords int                         `json:"totalRecords"`
	Data         []PhotgrapherProjectionView `json:"data"`
}
