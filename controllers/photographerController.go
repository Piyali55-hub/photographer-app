package controllers

import (
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/Piyali55-hub/photographer-app/repository"
	"gitlab.com/Piyali55-hub/photographer-app/view"
	"go.mongodb.org/mongo-driver/mongo"
)

type HttpError struct {
	Cause        string `json:"cause"`
	ErrorMessage string `json:"error_message"`
}

func NewError(c string, m string) *HttpError {
	return &HttpError{
		Cause:        c,
		ErrorMessage: m,
	}
}

type PhotographerController struct {
	PhotographerRepo repository.Repository
}

func NewPhotographerController(photographerRepo repository.Repository) *PhotographerController {
	return &PhotographerController{PhotographerRepo: photographerRepo}
}

// FindAllPhotographers godoc
// @Summary      Returns list of photographers
// @Description  get all photographers
// @Tags         photographers
// @Produce      json
// @Param        pageNumber   query      int  false  "Page Number"
// @Param        size   query      int  false  "Size"
// @Success      200  {object}  view.PhotographerAllView
// @Failure      400  {object}  HttpError
// @Failure      500  {object}  HttpError
// @Router       /photographers [get]
func (phController *PhotographerController) FindAll(c *gin.Context) {

	pageNum, err := strconv.Atoi(c.DefaultQuery("pageNumber", "1"))

	if err != nil {
		errorHandling(c, err, 400, "Page Number format wrong")
		return
	}
	size, err := strconv.Atoi(c.DefaultQuery("size", "5"))

	if err != nil {
		errorHandling(c, err, 400, "Size format wrong")
		return
	}
	photographers, count, err := phController.PhotographerRepo.FindAll(pageNum, size)

	if err != nil {
		errorHandling(c, err, 500, "Some error hanppened during processing")
		return
	}

	photographerAll := view.PhotographerAllView{
		TotalRecords: count,
		Page:         pageNum,
		Size:         size,
		Data:         photographers,
	}
	c.JSON(200, photographerAll)
}

// FindPhotographerById godoc
// @Summary      Returns photographer by photographerID
// @Description  get a singe photograpger by photographerID
// @Tags         photographers
// @Produce      json
// @Param        photographerID   path      string  true  "Photographer ID"
// @Success      200  {object}  entity.Photgrapher
// @Failure      400  {object}  HttpError
// @Failure      404  {object}  HttpError
// @Failure      500  {object}  HttpError
// @Router       /photographers/{photographerID} [get]
func (phController *PhotographerController) FindById(c *gin.Context) {
	photograpgerId, err := strconv.Atoi(c.Param("photographerID"))
	if err != nil {
		errorHandling(c, err, 400, "Wrong photographer ID format")
		return
	}

	photographer, err := phController.PhotographerRepo.FindById(photograpgerId)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			errorMsg := fmt.Sprintf("No document was found with the Id %v", photograpgerId)
			errorHandling(c, err, 404, errorMsg)
		} else {
			errorHandling(c, err, 500, "Some error hanppened during processing")
		}
		return
	}
	c.JSON(200, photographer)
}

// FindPhotographerByEventType godoc
// @Summary      Returns list of photographers by eventType
// @Description  Returns list of photographers by eventType
// @Tags         photographers
// @Produce      json
// @Param        eventType   path      string  true  "Event Type"
// @Success      200  {object}  view.PhotographerQueryView
// @Failure      500  {object}  HttpError
// @Router       /photographers/event/{eventType} [get]
func (phController *PhotographerController) FindByEventType(c *gin.Context) {
	eventType := c.Param("eventType")

	photographers, err := phController.PhotographerRepo.FindByEventType(eventType)

	if err != nil {
		errorHandling(c, err, 500, "Some error hanppened during processing")
		return
	}

	queryView := view.PhotographerQueryView{
		TotalRecords: len(photographers),
		Data:         photographers,
	}
	c.JSON(200, queryView)
}

func errorHandling(c *gin.Context, err error, httpStatus int, message string) {
	httpErr := NewError(err.Error(), message)
	c.JSON(httpStatus, httpErr)
}
