package controllers

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/Piyali55-hub/photographer-app/entity"
	"gitlab.com/Piyali55-hub/photographer-app/view"
	"go.mongodb.org/mongo-driver/mongo"
)

type MockPhotographerRepo struct {
	mock.Mock
}

func (mockRepo *MockPhotographerRepo) FindAll(pageNumber int, size int) ([]entity.Photgrapher, int, error) {
	args := mockRepo.Called(pageNumber, size)
	return args.Get(0).([]entity.Photgrapher), args.Get(1).(int), args.Error(2)
}

func (mockRepo *MockPhotographerRepo) FindById(photographerId int) (entity.Photgrapher, error) {
	args := mockRepo.Called(photographerId)
	return args.Get(0).(entity.Photgrapher), args.Error(1)
}

func (mockRepo *MockPhotographerRepo) FindByEventType(eventType string) ([]view.PhotgrapherProjectionView, error) {
	args := mockRepo.Called(eventType)
	return args.Get(0).([]view.PhotgrapherProjectionView), args.Error(1)
}

func TestFindAllSuccess(t *testing.T) {
	gin.SetMode(gin.TestMode)

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request = &http.Request{
		Header: make(http.Header),
	}

	u := url.Values{}
	url := &url.URL{}
	c.Request.URL = url
	u.Add("pageNumber", "1")
	u.Add("size", "2")
	c.Request.URL.RawQuery = u.Encode()

	repository := new(MockPhotographerRepo)
	controller := *NewPhotographerController(repository)

	p1 := entity.Photgrapher{
		Id:        4072,
		Uid:       "c7204a53-77be-4ba6-b446-8f92921060a0",
		Password:  "uDQXExV6pr",
		FirstName: "Lorenzo",
		LastName:  "Fay",
		Username:  "lorenzo.fay",
		Email:     "lorenzo.fay@email.com",
		Avatar:    "https://robohash.org/autconsecteturlabore.png?size=300x300&set=set1",
	}

	p2 := entity.Photgrapher{
		Id:        4073,
		Uid:       "e70098c4-ffe4-4c57-a508-c9f3d1a93822",
		Password:  "NWhCXfKbTU",
		FirstName: "Karey",
		LastName:  "Kuhic",
		Username:  "karey.kuhic",
		Email:     "karey.kuhic@email.com",
		Avatar:    "https://robohash.org/sedremquis.png?size=300x300&set=set1",
	}
	expected := []entity.Photgrapher{p1, p2}
	repository.On("FindAll", 1, 2).Return(expected, 4, nil)
	controller.FindAll(c)

	assert.EqualValues(t, http.StatusOK, w.Code)
	result := view.PhotographerAllView{}
	json.Unmarshal(w.Body.Bytes(), &result)
	assert.EqualValues(t, result.TotalRecords, 4)
	assert.EqualValues(t, len(result.Data), 2)
	assert.ElementsMatch(t, expected, result.Data)
}

func TestFindAllRepositoryError(t *testing.T) {
	gin.SetMode(gin.TestMode)

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request = &http.Request{
		Header: make(http.Header),
	}

	u := url.Values{}
	url := &url.URL{}
	c.Request.URL = url
	u.Add("pageNumber", "1")
	u.Add("size", "2")
	c.Request.URL.RawQuery = u.Encode()

	repository := new(MockPhotographerRepo)
	controller := *NewPhotographerController(repository)

	repository.On("FindAll", 1, 2).Return([]entity.Photgrapher{}, 0, mongo.ErrClientDisconnected)
	controller.FindAll(c)

	assert.EqualValues(t, http.StatusInternalServerError, w.Code)
}

func TestFindAllBadRequestPageNumber(t *testing.T) {
	gin.SetMode(gin.TestMode)

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request = &http.Request{
		Header: make(http.Header),
	}

	u := url.Values{}
	url := &url.URL{}
	c.Request.URL = url
	u.Add("pageNumber", "abc")
	u.Add("size", "2")
	c.Request.URL.RawQuery = u.Encode()

	repository := new(MockPhotographerRepo)
	controller := *NewPhotographerController(repository)

	controller.FindAll(c)

	assert.EqualValues(t, http.StatusBadRequest, w.Code)
}

func TestFindAllBadRequestSize(t *testing.T) {
	gin.SetMode(gin.TestMode)

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request = &http.Request{
		Header: make(http.Header),
	}

	u := url.Values{}
	url := &url.URL{}
	c.Request.URL = url
	u.Add("pageNumber", "1")
	u.Add("size", "abc")
	c.Request.URL.RawQuery = u.Encode()

	repository := new(MockPhotographerRepo)
	controller := *NewPhotographerController(repository)

	controller.FindAll(c)

	assert.EqualValues(t, http.StatusBadRequest, w.Code)
}

func TestFindByIdSuccess(t *testing.T) {
	gin.SetMode(gin.TestMode)

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request = &http.Request{
		Header: make(http.Header),
	}

	c.Params = []gin.Param{
		{
			Key:   "photographerID",
			Value: "802",
		},
	}

	repository := new(MockPhotographerRepo)
	controller := *NewPhotographerController(repository)

	p1 := entity.Photgrapher{
		Id:        802,
		Uid:       "c7204a53-77be-4ba6-b446-8f92921060a0",
		Password:  "uDQXExV6pr",
		FirstName: "Lorenzo",
		LastName:  "Fay",
		Username:  "lorenzo.fay",
		Email:     "lorenzo.fay@email.com",
		Avatar:    "https://robohash.org/autconsecteturlabore.png?size=300x300&set=set1",
	}

	repository.On("FindById", 802).Return(p1, nil)
	controller.FindById(c)

	assert.EqualValues(t, http.StatusOK, w.Code)

	result := entity.Photgrapher{}
	json.Unmarshal(w.Body.Bytes(), &result)
	assert.Equal(t, p1, result)
}

func TestFindByIdBadRequest(t *testing.T) {
	gin.SetMode(gin.TestMode)

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request = &http.Request{
		Header: make(http.Header),
	}

	c.Params = []gin.Param{
		{
			Key:   "photographerID",
			Value: "abc",
		},
	}

	repository := new(MockPhotographerRepo)
	controller := *NewPhotographerController(repository)

	controller.FindById(c)

	assert.EqualValues(t, http.StatusBadRequest, w.Code)
}

func TestFindByIdNoDocFound(t *testing.T) {
	gin.SetMode(gin.TestMode)

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request = &http.Request{
		Header: make(http.Header),
	}

	c.Params = []gin.Param{
		{
			Key:   "photographerID",
			Value: "802",
		},
	}

	repository := new(MockPhotographerRepo)
	controller := *NewPhotographerController(repository)

	p1 := entity.Photgrapher{}

	repository.On("FindById", 802).Return(p1, mongo.ErrNoDocuments)
	controller.FindById(c)

	assert.EqualValues(t, http.StatusNotFound, w.Code)
}

func TestFindByIdDBError(t *testing.T) {
	gin.SetMode(gin.TestMode)

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request = &http.Request{
		Header: make(http.Header),
	}

	c.Params = []gin.Param{
		{
			Key:   "photographerID",
			Value: "802",
		},
	}

	repository := new(MockPhotographerRepo)
	controller := *NewPhotographerController(repository)

	p1 := entity.Photgrapher{}

	repository.On("FindById", 802).Return(p1, mongo.ErrClientDisconnected)
	controller.FindById(c)

	assert.EqualValues(t, http.StatusInternalServerError, w.Code)
}

func TestFindByEventTypeSuccess(t *testing.T) {
	gin.SetMode(gin.TestMode)

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request = &http.Request{
		Header: make(http.Header),
	}

	c.Params = []gin.Param{
		{
			Key:   "eventType",
			Value: "wedding",
		},
	}

	repository := new(MockPhotographerRepo)
	controller := *NewPhotographerController(repository)

	p1 := view.PhotgrapherProjectionView{
		Id:        802,
		FirstName: "Lorenzo",
		LastName:  "Fay",
		Avatar:    "https://robohash.org/autconsecteturlabore.png?size=300x300&set=set1",
	}

	expected := []view.PhotgrapherProjectionView{p1}
	repository.On("FindByEventType", "wedding").Return(expected, nil)
	controller.FindByEventType(c)

	assert.EqualValues(t, http.StatusOK, w.Code)

	result := view.PhotographerQueryView{}
	json.Unmarshal(w.Body.Bytes(), &result)
	assert.EqualValues(t, result.TotalRecords, 1)
	assert.EqualValues(t, len(result.Data), 1)
	assert.ElementsMatch(t, expected, result.Data)
}

func TestFindByEventTypeError(t *testing.T) {
	gin.SetMode(gin.TestMode)

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request = &http.Request{
		Header: make(http.Header),
	}

	c.Params = []gin.Param{
		{
			Key:   "eventType",
			Value: "wedding",
		},
	}

	repository := new(MockPhotographerRepo)
	controller := *NewPhotographerController(repository)

	repository.On("FindByEventType", "wedding").Return([]view.PhotgrapherProjectionView{}, mongo.ErrClientDisconnected)
	controller.FindByEventType(c)

	assert.EqualValues(t, http.StatusInternalServerError, w.Code)
}
