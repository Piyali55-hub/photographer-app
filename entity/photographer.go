package entity

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

type Photgrapher struct {
	Id                    int                 `json:"id" bson:"_id"`
	Uid                   string              `json:"uid"`
	Password              string              `json:"password"`
	FirstName             string              `json:"first_name" bson:"first_name"`
	LastName              string              `json:"last_name" bson:"last_name"`
	Username              string              `json:"username"`
	Email                 string              `json:"email"`
	Avatar                string              `json:"avatar"`
	Gender                string              `json:"gender"`
	PhoneNumber           string              `json:"phone_number" bson:"phone_number"`
	SocialInsuranceNumber string              `json:"social_insurance_number" bson:"social_insurance_number"`
	DateOfBirth           string              `json:"date_of_birth" bson:"date_of_birth"`
	EventType             map[string][]string `json:"event_type" bson:"event_type"`
	Address               map[string]any      `json:"address"`
	CreditCard            map[string]string   `json:"credit_card" bson:"credit_card"`
	Subscription          map[string]string   `json:"subscription"`
}

func GetData() (photographers []Photgrapher) {
	photographersData, err := os.Open("photographers.json")
	defer photographersData.Close()
	if err != nil {
		fmt.Println("Could not open the photographers.json file", err.Error())
		log.Panic(err)
	}

	err = json.NewDecoder(photographersData).Decode(&photographers)
	if err != nil {
		fmt.Println("Could not decode the photographers.json data", err.Error())
		log.Panic(err)
	}

	return photographers
}
