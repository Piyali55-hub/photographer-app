# Photographer App

This is a photographer app.

## How to start the app locally

- Install and run docker in your machine
- Download code from this repository using `git clone https://gitlab.com/Piyali55-hub/photographer-app.git`
- Go to project directory
- Run `docker-compose up -d`

## How to test the app locally

Once docker container is up you can open browser and navigate to swagger using below link:

 [Swagger UI for the App](http://localhost:3000/swagger/index.html)

You can use swagger UI to read API documentation and test all the APIs exposed by the APP.
