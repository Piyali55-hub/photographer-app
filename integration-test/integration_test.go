package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/ory/dockertest/v3"
	"github.com/ory/dockertest/v3/docker"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Piyali55-hub/photographer-app/entity"
	"gitlab.com/Piyali55-hub/photographer-app/view"
)

var port string

func TestMain(m *testing.M) {
	// Setup
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	network, err := pool.CreateNetwork("backend")
	if err != nil {
		log.Fatalf("Could not create Network to docker: %s \n", err)
	}

	networks := []*dockertest.Network{network}

	mongoCtnr, err := startMongoDb(pool, networks)
	if err != nil {
		cleanUp(1, pool, network, mongoCtnr)
	}

	time.Sleep(1 * time.Second)

	apiCtnr, err := startAPI(pool, networks)
	if err != nil {
		cleanUp(1, pool, network, mongoCtnr, apiCtnr)
	}

	port = apiCtnr.GetPort("3000/tcp")

	time.Sleep(1 * time.Second)
	exitCode := m.Run()

	defer cleanUp(exitCode, pool, network, mongoCtnr, apiCtnr)
}

func startMongoDb(pool *dockertest.Pool, network []*dockertest.Network) (*dockertest.Resource, error) {
	environmentVariables := []string{
		"MONGO_INITDB_ROOT_USERNAME=" + "michael",
		"MONGO_INITDB_ROOT_PASSWORD=" + "secret",
	}
	resource, err := pool.RunWithOptions(&dockertest.RunOptions{
		Name:       "mongodb",
		Repository: "mongo",
		Tag:        "5.0",
		Env:        environmentVariables,
		Networks:   network,
		Mounts:     []string{"/Users/piyalidas/Golang/photographer-app/mongo-init.js:/docker-entrypoint-initdb.d/mongo-init.js:ro"},
	}, func(config *docker.HostConfig) {
		// set AutoRemove to true so that stopped container goes away by itself
		config.AutoRemove = true
		config.RestartPolicy = docker.RestartPolicy{
			Name: "no",
		}
	})
	if err != nil {
		log.Fatalf("Could not start resource: %s", err)
	}
	return resource, err
}

func startAPI(pool *dockertest.Pool, network []*dockertest.Network) (*dockertest.Resource, error) {

	envs := []string{
		"DB_URL=mongodb://michael:secret@mongodb:27017/",
	}
	apiContainerName := "go-integration-test"

	r, err := pool.BuildAndRunWithBuildOptions(
		&dockertest.BuildOptions{
			ContextDir: "../",
			Dockerfile: "Dockerfile",
		},
		&dockertest.RunOptions{
			Name:     apiContainerName,
			Env:      envs,
			Networks: network,
		})
	if err != nil {
		fmt.Printf("Could not start %s: %v \n", apiContainerName, err)
	}
	return r, err
}

func cleanUpResources(resources []*dockertest.Resource, pool *dockertest.Pool) {
	fmt.Println("removing resources.")
	for _, resource := range resources {
		if resource != nil {
			if err := pool.Purge(resource); err != nil {
				log.Fatalf("Could not purge resource: %s\n", err)
			}
		}
	}
}

func cleanUp(code int, pool *dockertest.Pool, network *dockertest.Network, resources ...*dockertest.Resource) {
	cleanUpResources(resources, pool)
	network.Close()
	os.Exit(code)
}

func TestFindAll_SuccessScenario(t *testing.T) {
	url := fmt.Sprintf("http://localhost:%s/api/photographers/", port)
	fmt.Println("calling application on : ", url)

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		t.Fail()
	}

	resp, err := http.DefaultClient.Do(request)

	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)

	bytes, _ := io.ReadAll(resp.Body)

	response := view.PhotographerAllView{}
	json.Unmarshal(bytes, &response)
	assert.NotEmpty(t, response.Data)
	assert.Nil(t, err)
}

func TestFindById_SuccessScenario(t *testing.T) {
	id := 4072
	url := fmt.Sprintf("http://localhost:%s/api/photographers/%v", port, id)
	fmt.Println("calling application on : ", url)

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		t.Fail()
	}

	resp, err := http.DefaultClient.Do(request)

	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)

	bytes, _ := io.ReadAll(resp.Body)

	var photographer entity.Photgrapher
	json.Unmarshal(bytes, &photographer)
	assert.Equal(t, id, photographer.Id)
	assert.Nil(t, err)
}

func TestFindById_FailureScenario(t *testing.T) {
	id := 1
	url := fmt.Sprintf("http://localhost:%s/api/photographers/%v", port, id)
	fmt.Println("calling application on : ", url)

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		t.Fail()
	}

	resp, err := http.DefaultClient.Do(request)

	assert.Nil(t, err)
	assert.Equal(t, http.StatusNotFound, resp.StatusCode)

	bytes, _ := io.ReadAll(resp.Body)

	var response map[string]string
	json.Unmarshal(bytes, &response)
	assert.NotEmpty(t, response["error_message"])
	assert.Nil(t, err)
}

func TestFindByEventType_SuccessScenario(t *testing.T) {
	eventType := "wedding"
	url := fmt.Sprintf("http://localhost:%s/api/photographers/event/%s", port, eventType)
	fmt.Println("calling application on : ", url)

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		t.Fail()
	}

	resp, err := http.DefaultClient.Do(request)

	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)

	bytes, _ := io.ReadAll(resp.Body)

	response := view.PhotographerQueryView{}
	json.Unmarshal(bytes, &response)
	assert.Equal(t, 19, len(response.Data))
	assert.Nil(t, err)
}

func TestFindByEventType_EmptyResponseScenario(t *testing.T) {
	eventType := "wedding1"
	url := fmt.Sprintf("http://localhost:%s/api/photographers/event/%s", port, eventType)
	fmt.Println("calling application on : ", url)

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		t.Fail()
	}

	resp, err := http.DefaultClient.Do(request)

	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)

	bytes, _ := io.ReadAll(resp.Body)

	response := view.PhotographerQueryView{}
	json.Unmarshal(bytes, &response)
	assert.Empty(t, response.Data)
	assert.Nil(t, err)
}
